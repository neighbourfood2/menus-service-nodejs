"use strict";

module.exports = function(sequelize, DataTypes) {
  var DeliveryArea = sequelize.define("DeliveryArea", {
    name:      DataTypes.STRING,
    latitude:  DataTypes.DECIMAL(10, 6),
    longitude: DataTypes.DECIMAL(10, 6),
    radius:    DataTypes.INTEGER,
    address:   DataTypes.TEXT,
    notes:     DataTypes.TEXT,
    is_archived:  DataTypes.BOOLEAN,
    household_id: DataTypes.INTEGER
  }, {
    tableName: 'delivery_areas',
    timestamps: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      },
      instanceMethods: {

      }
    }
  });

  //////////

  return DeliveryArea;
};
