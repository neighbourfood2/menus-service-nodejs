"use strict";

module.exports = function(sequelize, DataTypes) {
  var Dish = sequelize.define("Dish", {
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    
    image_file_name: DataTypes.STRING,
    image_content_type: DataTypes.STRING,
    image_file_size: DataTypes.INTEGER,
    image_updated_at: DataTypes.DATE,

    calories_level: DataTypes.STRING,
    fat_level: DataTypes.INTEGER,
    spice_level: DataTypes.INTEGER,

    is_archived: DataTypes.BOOLEAN
  }, {
    tableName: 'dishes',
    timestamps: true,
    classMethods: {
      associate: associate
    },
    instanceMethods: {

    }
  });

  //////////
  
  function associate(models) {
    //has_many :menus, through: :menu_dishes

    Dish.belongsToMany(models.Menu, {
      through: {
        model: models.MenuDish,
        unique: false
      },
      foreignKey: 'menu_id'
    });
  }

  return Dish;
};