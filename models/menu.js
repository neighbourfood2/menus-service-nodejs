"use strict";

module.exports = function(sequelize, DataTypes) {
  var Menu = sequelize.define("Menu", {
    raw_price:    DataTypes.DECIMAL(10, 2),
    quantity:     DataTypes.INTEGER,
    total_left:   DataTypes.INTEGER,
    delivery_eta: DataTypes.DATE,
    description:  DataTypes.TEXT,
    is_archived:  DataTypes.BOOLEAN,
    charge:       DataTypes.DECIMAL(10, 2),
    block_sale:   DataTypes.BOOLEAN,
    main_dish_id: DataTypes.INTEGER,
    household_id: DataTypes.INTEGER
  }, {
    tableName: 'menus',
    timestamps: true,
    classMethods: {
      associate: associate
    },
    instanceMethods: {

    }
  });

  //////////

  function associate(models) {
    // belongs_to :delivery_area
    // has_many :dishes, through: :menu_dishes

    Menu.belongsTo(models.DeliveryArea);
    
    Menu.belongsToMany(models.Dish, {
      through: {
        model: models.MenuDish,
        unique: false
      },
      foreignKey: 'dish_id'
    });
  }

  return Menu;
};