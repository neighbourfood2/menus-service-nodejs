"use strict";

module.exports = function(sequelize, DataTypes) {
  var MenuDish = sequelize.define("MenuDish", {
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,

    menu_id: DataTypes.INTEGER,
    dish_id: DataTypes.INTEGER
  }, {
    tableName: 'menu_dishes',
    timestamps: true,
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      },
      instanceMethods: {

      }
    }
  });

  //////////

  return MenuDish;
};