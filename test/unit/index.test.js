'use strict';

var expect = require('expect.js');

describe('models/index', function () {
  it('returns the menu model', function () {
    var models = require('../../models');
    expect(models.Menu).to.be.ok();
  });

  it('returns the dish model', function () {
    var models = require('../../models');
    expect(models.Dish).to.be.ok();
  });

  it('returns the menu_dish model', function () {
    var models = require('../../models');
    expect(models.MenuDish).to.be.ok();
  });

  it('returns the delivery_area model', function () {
    var models = require('../../models');
    expect(models.DeliveryArea).to.be.ok();
  });
});