'use strict';

var expect = require('expect.js');

describe('models/dish', function () {
  beforeEach(function () {
    this.Dish = require('../../models').Dish;
  });

  describe('create', function () {
    it('creates a dish', function () {
      return this.Dish.create({ 
        name: 'dish name', 
        description: 'dish description', 

        image_file_name: 'image.jps',
        image_content_type: 'jpg',
        image_file_size: '300',
        image_updated_at: '2015-10-02',

        calories_level: 5,
        fat_level: 5,
        spice_level: 5,

        is_archived: false}).then(function (dish) {
          expect(dish.name).to.equal('dish name');
          expect(dish.description).to.equal('dish description');

          expect(dish.image_file_name).to.equal('image.jps');
          expect(dish.image_content_type).to.equal('jpg');
          expect(dish.image_file_size).to.equal('300');
          //expect(dish.image_updated_at).to.equal('2015-10-02');

          expect(dish.calories_level).to.equal(5);
          expect(dish.fat_level).to.equal(5);
          expect(dish.spice_level).to.equal(5);

          expect(dish.is_archived).to.equal(false);
        });
    });
  });
});