'use strict';

module.exports = {

  up: function (queryInterface, DataTypes) {
    return queryInterface.createTable('menus', 
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      raw_price:    DataTypes.DECIMAL(10, 2),
      quantity:     DataTypes.INTEGER,
      total_left:   DataTypes.INTEGER,
      delivery_eta: DataTypes.DATE,
      description:  DataTypes.TEXT,
      is_archived:  DataTypes.BOOLEAN,
      charge:       DataTypes.DECIMAL(10, 2),
      block_sale:   DataTypes.BOOLEAN,
      main_dish_id: DataTypes.INTEGER,
      household_id: DataTypes.INTEGER,
      created_at: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: function (queryInterface, DataTypes) {
    return queryInterface.dropTable('menus');
  }
};
