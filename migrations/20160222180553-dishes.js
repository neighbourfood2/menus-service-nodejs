'use strict';

module.exports = {

  up: function (queryInterface, DataTypes) {
    return queryInterface.createTable('dishes', 
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      
      image_file_name: DataTypes.STRING,
      image_content_type: DataTypes.STRING,
      image_file_size: DataTypes.INTEGER,
      image_updated_at: DataTypes.DATE,

      calories_level: DataTypes.STRING,
      fat_level: DataTypes.INTEGER,
      spice_level: DataTypes.INTEGER,

      is_archived: DataTypes.BOOLEAN,
      created_at: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: function (queryInterface, DataTypes) {
    return queryInterface.dropTable('dishes');
  }
};
