'use strict';

module.exports = {

  up: function (queryInterface, DataTypes) {
    return queryInterface.createTable('delivery_areas', 
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      name:      DataTypes.STRING,
      latitude:  DataTypes.DECIMAL(10, 6),
      longitude: DataTypes.DECIMAL(10, 6),
      radius:    DataTypes.INTEGER,
      address:   DataTypes.TEXT,
      notes:     DataTypes.TEXT,
      is_archived:  DataTypes.BOOLEAN,
      household_id: DataTypes.INTEGER,
      created_at: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: function (queryInterface, DataTypes) {
    return queryInterface.dropTable('delivery_areas');
  }
};
