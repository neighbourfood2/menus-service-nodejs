'use strict';

module.exports = {

  up: function (queryInterface, DataTypes) {
    return queryInterface.createTable('menu_dishes', 
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      menu_id: DataTypes.INTEGER,
      dish_id: DataTypes.INTEGER,

      created_at: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updated_at: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },

  down: function (queryInterface, DataTypes) {
    return queryInterface.dropTable('menu_dishes');
  }
};
