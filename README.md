# Menus Service Node.js Implementation

## Synopsis

Menus services is a microservice for the menu resources and its main asociations
as delivery_areas and dishes.

## Features List

1. API endpoints for:

  * CRUD menus/
  * CRUD dishes/
  * CRUD delivery_areas/
  * GET search/

2. Isolated database for menus, dishes, delivery_areas

3. Oauth 2 secured

4. Microservice properties: 

  * Single responsability
  * High decoupling
  * Can be instantiated independently

5.  By being implemented in node we expect high concurrency, good performance 
and low memory footprint.

## Development

### Contact

* Lead Architect: [saul.ortigoza@sortigoza.com](mailto:saul.ortigoza@sortigoza.com)
* NeighbourFood website: [neighbourfood.mx](http://www.neighbourfood.mx/contact)
* NeighbourFood Contact: [contacto@neighbourfood.mx](mailto:contacto@neighbourfood.mx)

### Documentation

* ./docs
* [Repo](https://bitbucket.org/neighbourfood2/menus-service-nodejs)

### Testing

* Mocha
* ./test
* [Bug incidents](https://bitbucket.org/neighbourfood2/menus-service-nodejs/issues)

### Project Management

* [Scrumban board](https://trello.com/b/bviggIVA/neighbourfood-app)

### Servers

* Test server: [test.neighbourfood.mx/](http://test.neighbourfood.mx/)
* Production server: [neighbourfood.mx/](http://neighbourfood.mx/)

### Deployment

To do deployment by Capistrano

## Changelog

See {file:CHANGELOG.md} for a list of changes.

## License

Please see the {file:LICENSE} and {file:LEGAL} documents for more information.